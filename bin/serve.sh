#!/bin/bash

GSD5_PATH=`pwd`
TIDDLYWIKI5_PATH=$GSD5_PATH/../TiddlyWiki5

export TIDDLYWIKI_PLUGIN_PATH=$GSD5_PATH/plugins
export TW_SERVE_EDITION_PATH=$GSD5_PATH/editions/gsd5-server
( cd $TIDDLYWIKI5_PATH && ./bin/serve.sh )