#!/bin/bash

GSD5_PATH=`pwd`

TIDDLYWIKI_PATH=$GSD5_PATH/../TiddlyWiki5
export TIDDLYWIKI_PLUGIN_PATH=$GSD5_PATH/plugins

OUTPUT_PATH=$GSD5_PATH/../gsd5-docs-output
if [ -d $OUTPUT_PATH ]; then
	mkdir -p $OUTPUT_PATH
fi

( node $TIDDLYWIKI_PATH/tiddlywiki.js \
	$GSD5_PATH/editions/gsd5-docs \
	--verbose \
	--output $OUTPUT_PATH \
	--build )

echo Wrote files to $OUTPUT_PATH/